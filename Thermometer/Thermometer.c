#include <avr/io.h>
#define F_CPU 8500000L
#include <util/delay.h>

#define T_PORT PORTA
#define T_DDR DDRA
#define T_PIN PINA
#define T 0

#define LCD_COMMAND_DDR DDRB
#define LCD_COMMAND_PORT PORTB
#define LCD_RS 2
#define LCD_RW 3
#define LCD_E 4

char GetBit(char val, char bitNumber)
{
	if ((val & (1<<bitNumber)) == 0)
	return 0;
	else
	return 1;
}

void SetBit(char target, char bitNumber, char val)
{
	if (val == 0)
	*(char*)target &= ~(1<<bitNumber);
	else
	*(char*)target |= (1<<bitNumber);
}

void assign_data(char d)
{
	for (char i=0; i<=5; i++)
	SetBit(&PORTD, i, GetBit(d,i));
	
	SetBit(&PORTD, 6, GetBit(d,7));
	SetBit(&PORTB, 0, GetBit(d,6));
}

void data(char cmd)
{
	LCD_COMMAND_PORT |= (1<<LCD_E) | (1<<LCD_RS); //����� ������, ����� ������
	assign_data(cmd); // ����� ������
	LCD_COMMAND_PORT &= ~(1<<LCD_E); // ����� �����, ������� ������!
	_delay_ms(5);  // ���� ���� �������
	
	assign_data(0xFF);
	LCD_COMMAND_PORT &= ~(1<<LCD_RS); // ������� ������� ������
	_delay_ms(5);
}

void command(char cmd)
{
	LCD_COMMAND_PORT |= (1<<LCD_E); // ���� ���� �������
	assign_data(cmd); // ����� ������
	LCD_COMMAND_PORT &= ~(1<<LCD_E); // ����� �����
	assign_data(0xFF);
	_delay_ms(5);
}


void init_display(void)
{
	LCD_COMMAND_DDR |= (1<<LCD_E) | (1<<LCD_RW) | (1<<LCD_RS);
	for (char i=0; i<=6; i++)
	SetBit(&DDRD, i, 1);
	SetBit(&DDRB, 0, 1);
	
	_delay_ms(15);
	command(0b00111000);
	command(0b00000001);
	command(0b00000110);
	
	command(0b00001100);
	command(0b00000001);
	return;
}

void lcd_write(char *str)
{
	while(*str != '\0')
	{
		data(*str);
		str++;
	}
}


int reset_t()
{
	int isPresent = 0;
	T_DDR |= (1<<T);
	T_PORT &= ~(1<<T);
	_delay_us(480);
	
	T_DDR &= ~(1<<T);
	_delay_us(60);
	
	if ((T_PIN & (1<<T)) == 0){
		isPresent = 1;
	}
	
	_delay_us(480);
	return isPresent;
}



void write0()
{
	T_DDR |= (1<<T); // ����� ���� �� ��������
	
	T_PORT &= ~(1<<T); // ����������� ���� � 0
	_delay_us(85); // ���� 85 �����������
	
	T_DDR &= ~(1<<T); // ��������� ����
	
	_delay_us(5); // ����� ����� �������
}

void write1()
{
	T_DDR |= (1<<T); // ����� ���� �� ��������

	T_PORT &= ~(1<<T); // ����������� ���� � 0
	_delay_us(5); // ���� 5 �����������

	T_DDR &= ~(1<<T); // ��������� ����

	_delay_us(80); // �����
	_delay_us(5); // ����� ����� �������
}

char ReadBit()
{
	T_DDR |= (1<<T); // ����� ���� �� ��������
	T_PORT &= ~(1<<T); // ����������� ���� � 0
	_delay_us(5); // ���� 5 �����������
	
	T_DDR &= ~(1<<T); // ��������� ����
	_delay_us(5);
	if ((T_PIN & (1<<T)) == 0)
	{
		_delay_us(80);
		return 0;
	}
	else
	{
		_delay_us(80);
		return 1;
	}
	
}

void write(char val)
{
	for (int i = 0; i < 8; i++)
	{
		if (GetBit(val, i) == 0)
		write0();
		else
		write1();
	}
}

char* GetTemperature()
{
	reset_t();
	write(0xCC);
	write(0x44);
	
	//_delay_ms(1000);
	
	reset_t();
	write(0xCC);
	write(0xBE);
	
	signed char integer = 0;
	int decimal = 0;
	
	// ������ ������� �����
	// ����� ��������� ��� ������������� 0.0625
	if (ReadBit())
	{
		decimal += 625;
	}
	
	// ����� ������ ��� 0.125
	if (ReadBit())
	{
		decimal += 1250;
	}
	
	// ����� ������ ��� ������������� 0.25
	if (ReadBit())
	{
		decimal += 2500;
	}
	
	// ����� ������ ��� ������������� 0.5
	if (ReadBit())
	{
		decimal += 5000;
	}
		
	for (char i = 0; i < 8; i++)
	{
		integer >>= 1;
		integer &= 0b01111111;
		if (ReadBit())
		{
			integer |= 0b10000000;
		}
	}
	
	char *tmp = malloc(sizeof(char) * 8);
	
	char *integerS = malloc(sizeof(char) * 3);
	itoa(integer, integerS, 10);
	
	char *decimalS = malloc(sizeof(char) * 4);
	itoa(decimal, decimalS, 10);
	
	strcat(tmp, integerS);
	strcat(tmp, ".");
	strcat(tmp, decimalS);
	
	strcat(tmp, "\0");
	
	free(integerS);
	free(decimalS);
	return tmp;
}

int main(void)
{	_delay_ms(1000);
	init_display();
	lcd_write("Init...");
	
	_delay_ms(1000);
	int i = 0;
	//command(0xC0); // ������� �� ���� ������?
		
	while (1)
	{
		char *temp = GetTemperature();
		
		command(0x01); // ������� �������
		lcd_write(temp);
		
		command(0xC0); // ������� �� ���� ������
		
		char *iteration = malloc(sizeof(char) * 8);
		itoa(i, iteration, 10);
		lcd_write(iteration);
		
		free(iteration);
		free(temp);
		
		i++;
		_delay_ms(5000);
	}
}