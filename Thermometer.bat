@ECHO OFF

SET current_directory=%cd%
set avrdude_tool=%current_directory%\..\FTBB\avrdude.exe
set hex_file=%current_directory%\Thermometer\Debug\Thernometer.hex

echo %avrdude_tool%
echo %hex_file%

rem D:\Electronics\FTBB\avrdude.exe -p t2313 -c ftbb -P ft0 -B 200 -U lfuse:w:0x64:m -U hfuse:w:0xdf:m
%avrdude_tool% -p t2313 -c ftbb -P ft0 -B 4800 -U flash:w:%hex_file%:a

pause